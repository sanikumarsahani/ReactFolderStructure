# Folder structure App
The project is deployed here. Open [https://sani2608.github.io/ReactFolderStructure/](https://sani2608.github.io/ReactFolderStructure/) 
#
## video link
The project is deployed here. Open [drive-link](https://drive.google.com/file/d/1XSjJUxGUTtnI5w4n3NmuNB5JA9O8yiF3/view?usp=sharing) 
## Available Scripts

In the project directory, you can run:

### `npm install` & `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

